#!/bin/bash -e
# Script to run on a vault server to upgrade vault configuration and binary 

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# vault script
mkdir -p /opt/etc/vault

GET=/opt/bin/s3get.sh

# downlaod vault configuration files
source /root/bootstrap/envs.sh
$GET ${CONFIG_BUCKET} vault/envvars /opt/etc/vault/envvars

# install vault binary
source /opt/etc/vault/envvars
docker run --rm -v /opt/bin:/tmp ${VAULT_IMAGE} cp /bin/vault /tmp/vault

# Restart vault
systemctl -o verbose stop vault
sleep 5
systemctl -o verbose start vault
