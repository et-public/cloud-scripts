#!/bin/bash -e
port=$1
if [[ ! -z "$port" ]] &&  pgrep -f $port &> /dev/null
then
    kill $(pgrep -f $port  )
    echo Killed process on port $port
else
   echo "No process on port $port."
fi
exit 0
