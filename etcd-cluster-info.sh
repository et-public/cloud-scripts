#!/bin/bash -e

# Check etcd cluster health and print out vault active server.

source /etc/profile.d/vault.sh || true
PATH=$PATH:/opt/bin

# Prod vault lives in vault.stanford.edu zone, dev lives in itlab.stanford.edu zone
VAULT_ADDR=https://${ROUTE53_ZONE_NAME}
if [ "X${ROUTE53_ZONE_NAME}" != "Xvault.stanford.edu" ]
then
    VAULT_ADDR=https://vault.${ROUTE53_ZONE_NAME}
fi
endpoint=$1
leader="NONE"

# Override endpoint if the vault cluster is already setup
if vault status --address=$VAULT_ADDR | grep -i "HA Cluster" ; then
    leader=$(vault status --address=$VAULT_ADDR | grep -i "HA Cluster" | awk '{print $NF}' | sed -e 's%https:%http:%;s%:8201%%')
    endpoint=${leader}
fi

echo ""
etcdctl --endpoint=$endpoint:2379 cluster-health

echo ""
etcdctl --endpoint=$endpoint:2379 member list

echo ""
echo "Vault leader is $leader."