#!/bin/bash -e

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
CLUSTER=${1:-'vault-prod'}

if ! $DIR/vault-status.sh $CLUSTER | grep -i sealed |grep -q -i true; then
  echo "All vault servers in $CLUSTER are already unsealed. Nothing to do."
  exit 0
fi

USER=${2:-$USER}
echo "Your USER environment variable is: $USER."
echo "Continue to unseal $CLUSTER, using $USER's GPG key ? [Y/N]: "
read ANSWER
if [ ! "$ANSWER" = "Y" ]; then \
    echo "Exiting." && exit 1
elif ! gpg --list-secret-keys $USER ; then
    echo "Private key for $USER doesn't exist on this compauter." && exit 1
fi

export USER=$USER
TMPDIR=$(mktemp -d /tmp/vault-unseal.XXXX)
pushd $TMPDIR > /dev/null
git clone --quiet git@code.stanford.edu:et/vault-service
cd vault-service
make clone
make unlock
cd sub-projects/vault-provision/$CLUSTER
make vault-status | grep -v ^Checking | grep -v ^Running > $TMPDIR/vault-status.0
# Select the first one that is sealed
jq -r '. | select(.sealed==true)' $TMPDIR/vault-status.0  2> /dev/null | jq -s '.[0]' > $TMPDIR/sealed-status.0

if  ! grep -q null $TMPDIR/sealed-status.0 ; then
  unseal_progress="No"
  make vault-unseal
  make vault-status | grep -v ^Checking | grep -v ^Running > $TMPDIR/vault-status.1 
  jq -r '. | select(.sealed==true)' $TMPDIR/vault-status.1  2> /dev/null | jq -s '.[0]' > $TMPDIR/sealed-status.1
  if  ! grep -q null $TMPDIR/sealed-status.1 ; then
    unseal_progress=$(jq -r '.progress' $TMPDIR/sealed-status.1)
    if [ $unseal_progress -gt 0 ]
    then
      unseal_thread=$(jq -r '.t' $TMPDIR/sealed-status.1) 
      unseal_needed=$(expr $unseal_thread - $unseal_progress)
    fi
  fi
  subj="$USER unsealed vault. $unseal_progress more is needed."
  echo $subj
  echo "Sending email through cardinal. SUNetID authentication is required."
  slack_addr=$(cat $TMPDIR/vault-service/sub-projects/vault-sec/${CLUSTER}/slack-address)
  ssh cardinal.stanford.edu "mail -s \"$subj.\" $slack_addr < /dev/null"
fi
echo "All done. Removing $TMPDIR"
rm -rf $TMPDIR/*

popd > /dev/null
