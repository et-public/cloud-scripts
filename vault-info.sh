#!/bin/bash
# You can pass multiple vault servers on the command line like so:
#
# ./vault-info.sh https://vault0.example.comdu:8200 https://vault1.example.com:8200 ..

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/utils/functions.sh

vault_info() {
    VAULT_ADDRS=${*:-https://vault.stanford.edu}

    for i in $VAULT_ADDRS
    do
      echo "VAULT SERVER: $i. Please wait ..."
      # seal status (0 unsealed, 2 sealed, 1 error)
      VAULT_ADDR=$i vault status
      [[ $? -eq 1 ]] && abort 1 "Error checking vault status."
      echo ""
    done
}
vault_info $@

