#!/bin/bash
#
# Copy secrets from vault to files, the vault paths and file paths is defined in $1 file
# The script read a file with vault secret path, field to read,  and output file to save.
#
# The input file format is:
#   <vault_path> <file_path>
# Example: secret/projects/authnz/authnz-x/certs/idp.stanford.edu ${PWD}/certs/idp.stanford.edu.pem
#          secret/projects/authnz/authnz-x/certs/idp.stanford.edu ${PWD}/certs/idp.stanford.edu.key

function copy_sec() {
    path=$1
    file=$2
    
    [[ -z "$path" ]] || [[ -z "$file" ]] && return

    format=$(vault kv get -field=format $path 2> /dev/null)

    mkdir -p $(dirname $2)
    #echo "vault kv get -field=value $path" 
    if [ "base64" == "$format" ]
    then
        vault kv get -field=value $path | base64 -D > $2
    else
        vault kv get -field=value $path > $2
    fi
}

input=$1
if [[ -z $input ]] || [[ ! -f $input ]]; then
  echo "Input file doesn't exist."
  exit 1
fi
grep -v '^#' $input | grep -v -e '^$' | envsubst |
while read -r line; do
    copy_sec $line
done
