#!/bin/bash -e

# You need to run this code on a machine that has wallet, netdb, and swhois, sucb as cardinal.stanford.edu
# Your kerberos ticket is required.

node=$1
template=$2

abort(){
  return_code=$1; shift
  message=$*
  echo "$message" && return $return_code
}

if [[ "X$node" = "X" ]]; then
  abort 1 "Node name must be provided."
fi

if ! swhois $node | grep -q name: && [[ "X$template" = "X" ]]; then
  echo "$node does not exist in netdb. Please provide a template node to create the node."
  abort 1 "Usage: $(basename $0) new-node-name template-node-name"
fi

if [[ "X$template" != "X" ]] && ! swhois $template | grep -q name: ; then
  abort 1 "template node $template does not exist." 
fi
if ! swhois $node | grep -q name: ; then
  echo "Running netdb node clone --template $template --name $node"
  /usr/bin/netdb node clone --template $template --name $node
fi
wallet -s wallet.stanford.edu get duo-pam $node
