#!/bin/bash -e -x
#
# Take SSL certificate key and crt from kubernetes sevice secret object, upload to AWS, reconfigure ELB
#

set -u
AWS_PROFILE=${AWS_PROFILE}
KUBECONFIG=${KUBECONFIG}
APP_NAMESPACE=${APP_NAMESPACE}
SERVICE=${APP}
set +u

# Get AWS ELB, CERT information
aws_account=$(aws sts get-caller-identity | jq -r '.Account')
json_output=/tmp/${APP}-svc.json
kubectl get svc ${APP} -n ${APP_NAMESPACE} -o json > $json_output
fqdn=$(cat $json_output | jq  -r '.metadata.annotations."external-dns.alpha.kubernetes.io/hostname"')
current_cert_arn=$(cat $json_output | jq -r '.metadata.annotations."service.beta.kubernetes.io/aws-load-balancer-ssl-cert"')
current_server_cert_name=$(basename $current_cert_arn)
old_cert_arn=$(cat $json_output | jq -r '.metadata.annotations."service.beta.kubernetes.io/aws-load-balancer-ssl-cert-old"')
old_server_cert_name=$(basename $old_cert_arn)
shortname=${fqdn%%.*}

# Upload new cert
# Get kube cert from kubernetes secret
kubectl get secret $shortname -n $APP_NAMESPACE -o json | jq -r '.data."tls.crt"' | base64 -D  > /tmp/$fqdn.crt
kubectl get secret $shortname -n $APP_NAMESPACE -o json | jq -r '.data."tls.key"' | base64 -D  > /tmp/$fqdn.key
if grep -q -- "-----BEGIN CERTIFICATE" /tmp/$fqdn.crt && \
    grep -q  -- "-----BEGIN RSA PRIVATE KEY" /tmp/$fqdn.key
then
  # Append server cert name with md5 of the cert
  cert_md5=$(md5  -q /tmp/$fqdn.crt)
  new_server_cert_name=${fqdn}-${cert_md5}
  new_cert_arn="arn:aws:iam::${aws_account}:server-certificate/${new_server_cert_name}"
  
  # Only update if certs are different
  if [ "$current_server_cert_name" = "${new_server_cert_name}" ]
  then
    echo "No change on cert."
    exit 0
  else
    aws iam upload-server-certificate --server-certificate-name ${new_server_cert_name} \
      --certificate-body file:///tmp/$fqdn.crt --private-key file:///tmp/$fqdn.key
    # Update annotation the service with new cert and configure ELB
    kubectl annotate --overwrite svc ${APP} -n ${APP_NAMESPACE} \
      service.beta.kubernetes.io/aws-load-balancer-ssl-cert="${new_cert_arn}"
    # Update annotation the service with new cert and configure ELB
    kubectl annotate --overwrite svc ${APP} -n ${APP_NAMESPACE} \
      service.beta.kubernetes.io/aws-load-balancer-ssl-cert-old="${current_cert_arn}"
    fi
else
  echo "Cert files are invalid."
  exit 1
fi

# Delete old cert
sleep 20
aws iam delete-server-certificate --server-certificate-name $old_server_cert_name || true

#rm -rf   /tmp/$fqdn.key  /tmp/$fqdn.crt
exit 0
