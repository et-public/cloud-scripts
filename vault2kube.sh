#!/bin/bash
# Usage:
#   vault2kube <secret.yml>
# Replace %%vaule-secret-path%% in a kube secret template file with 
# secret value stored at vaule-secret-path

OLDIFS=$IFS
IFS=''
while read line
do
    path=$(echo $line | grep -o -e '%%.*%%' | tr -d '%')
    if [ -z $path ]
    then
        echo $line
    else
        format=$(vault kv get -field=format $path 2> /dev/null)
    if [ "base64" == "$format" ]
    then
        sec=$(vault kv get -field=value $path)
    else
        sec=$(vault kv get -field=value $path | base64)
    fi
    echo $line | sed -e "s|%%${path}%%|${sec}|"
  fi
done < "${1:-/dev/stdin}"
IFS=$OLDIFS
