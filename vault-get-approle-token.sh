#!/bin/bash

###############################################################################
# Get vault approle id, secret and token
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/utils/functions.sh

trap_errors

set -u # must set vars
export VAULT_ROLE_NAME=${VAULT_ROLE_NAME}
set +u # optional vars
export VAULT_ADDR=${VAULT_ADDR:-https://vault.stanford.edu}
export VAULT_SEC_PATH=${VAULT_SEC_PATH:-auth/token/lookup-self}
export VAULT_AUTH_PATH=${VAULT_AUTH_PATH:-ldap}
export VAULT_AUTH_METHOD=${VAULT_AUTH_METHOD:-ldap}
export VAULT_ROLE_NAME=${VAULT_ROLE_NAME}
export VAULT_USER=${LOGNAME:-$USER} 

set -u

echo "VAULT ADDR: $VAULT_ADDR"

if ! vault token lookup > /dev/null 2>&1; then
    echo "Hey $VAULT_USER: Please login VAULT with DUO device ready. Waiting...:"
    vault login -method=${VAULT_AUTH_METHOD} -path=${VAULT_AUTH_PATH}
fi
role_id=$(vault read --format=json auth/approle/role/${VAULT_ROLE_NAME}/role-id | jq -r '.data.role_id')
secret_id=$(vault write --format=json -f auth/approle/role/${VAULT_ROLE_NAME}/secret-id | jq -r '.data.secret_id')
echo "Please set the role_id and secret_id to vault login. "
echo "export VAULT_ROLE_ID=$role_id"
echo "export VAULT_SECRET_ID=$secret_id"
