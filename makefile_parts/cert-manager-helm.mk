################################################################################
# cert-manager.mk
# See https://github.com/jetstack/cert-manager/blob/master/docs/user-guides/deploying.md
# See https://github.com/kubernetes/charts/blob/master/stable/cert-manager/README.md
################################################################################

ifndef ACME_URL
	export ACME_URL := https://acme-staging.api.letsencrypt.org/directory
endif

ifndef CERT_MANAGER_NS
	export CERT_MANAGER_NS := kube-system
endif

# NOTE cert-manager is not currently support set up renew-before-days, but the defualt is 30 days.
# See https://github.com/jetstack/cert-manager/blob/ce9e5ede2bc6d8ccbc8e4db086f84c17e3f4d3af/docs/user-guides/acme-http-validation.md
ifndef CERT_RENEW_BEFORE_DAYS
	export CERT_RENEW_BEFORE_DAYS := 30
endif

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

# ifndef EXTERNAL_DNS_GOOGLE_PROJECT
# 	missing_vars := ${missing_vars} EXTERNAL_DNS_GOOGLE_PROJECT
# endif

# ifndef CA_COMMON_NAME
# 	CA_COMMON_NAME := "ca.med.stanford.edu"
# endif
# ifndef CA_KEY_PATH
# 	CA_KEY_PATH := ${SEC_PATH}/cert-issuer/${GCP_ENVIRONMENT}/ca.key
# endif
# ifndef CA_CERT_PATH
# 	CA_CERT_PATH := ${SEC_PATH}/cert-issuer/${GCP_ENVIRONMENT}/ca.crt
# endif

.PHONY: create-cert-manager-ns
create-cert-manager-ns: config-kube ## create cert-manager namespace
	@if ! kubectl get namespace ${CERT_MANAGER_NS} &> /dev/null ; then \
		kubectl create namespace ${CERT_MANAGER_NS} ; \
	fi

.PHONY: deploy-cert-manager
deploy-cert-manager: create-cert-manager-ns ## create the cert-manager
	helm install \
    	--name cert-manager \
    	--namespace ${CERT_MANAGER_NS} \
		--set extraArgs={--cluster-resource-namespace=${CERT_MANAGER_NS}} \
    	stable/cert-manager

.PHONY: update-cert-manager
update-cert-manager: create-cert-manager-ns ## upgradethe cert-manager
	helm upgrade  \
		--set extraArgs={--cluster-resource-namespace=${CERT_MANAGER_NS}} \
    	cert-manager stable/cert-manager

.PHONY: ls-cert-manager
ls-cert-manager: ## list the cert-manager
	@helm ls --all cert-manager

.PHONY: cert-manager-status
cert-manager-status: ## check cert-manager status
	@helm status cert-manager

.PHONY: destroy-cert-manager
destroy-cert-manager: config-kube ## destroy the  cert-manager
	@helm delete --purge cert-manager

##################
# Cert Issuers 
##################
.PHONY: deploy-issuers
deploy-issuers: KUBE_SEC_DEF_FILE = ${TEMPLATES}/cert-manager/secret.yaml
deploy-issuers: kube-sec ## create cert issuers
	@kube_apply.sh ${TEMPLATES}/cert-manager/cert-issuers.yaml

.PHONY: destroy-issuer
destroy-issuers: destroy-kube-sec ## destroy cert issuers
	@kube_delete.sh ${TEMPLATES}/cert-manager/cert-issuers.yaml

.PHONY: show-issuers
show-issuers: ## show cert issuers
	@kubectl describe clusterissuer,issuer --all-namespaces

.PHONY: create-issuer-ca
create-issuer-ca: vault-login ## Generate a Issuer CA private key and cert and save to vault
	openssl genrsa -out ca.key 2048
	openssl req -x509 -new -nodes -key ca.key -subj "/CN=${CA_COMMON_NAME}" -days 3650 -reqexts v3_req -extensions v3_ca -out ca.crt
	vault-write.sh ${CA_KEY_PATH} @ca.key
	vault-write.sh ${CA_CERT_PATH} @ca.crt
	rm -f ca.key ca.crt

## end of cert-manager.mk
