################################################################################
## config.mk
################################################################################

ifndef AWS_PROFILE
	missing_vars := ${missing_vars} AWS_PROFILE
endif

ifndef CLUSTER_NAME
	missing_vars := ${missing_vars} CLUSTER_NAME
endif

ifndef KUBECONFIG
	missing_vars := ${missing_vars} KUBECONFIG
endif

ifndef CONTEXT_NAME
	missing_vars := ${missing_vars} CONTEXT_NAME
endif

.PHONY: config-kube
config-kube: ## Coonfigure AWS EKS kubeconfig
	@kubectx ${CONTEXT_NAME}

.PHONY: revoke-kube
revoke-kube: # revoke just kubernetes credentials
	@rm -f ~/.kube/${CLUSTER_NAME}
## end of config.mk
