

################################################################################
## kube-addons.mk
# Makefile targets useful for adding system-wide addons
################################################################################

KUBECONFIG := ${HOME}/.kube/${CLUSTER_NAME}
AWS_PROFILE := ${AWS_PROFILE}
ifndef CLUSTER_NAME
	missing_vars := ${missing_vars} CLUSTER_NAME
endif

ifndef AWS_PROFILE
	missing_vars := ${missing_vars} AWS_PROFILE
endif

ifndef TILLER_IMAGE
	TILLER_IMAGE := gcr.io/kubernetes-helm/tiller:v2.9.1
endif

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

ifndef SA_NAME
	missing_vars := ${missing_vars} SA_NAME
endif

-include vault-login
-include config-kube

###################################
### Kubernetes dashboard provision
###################################
.PHONY: dashboard
dashboard: KUBERNETES_DASHBOARD=https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
dashboard: config-kube ## Start dashboard
	@echo $$HOME/.kube/${CLUSTER_NAME}
	@kubectl apply -f ${TEMPLATES}/dashboard/role-binding.yaml
	@kubectl apply -f ${TEMPLATES}/dashboard/service-account.yaml
	@kubectl apply -f ${KUBERNETES_DASHBOARD}
	# Patch dashboard token ttl from 15 minutes to 24 hours.
	@kubectl patch deployment kubernetes-dashboard --patch "$$(cat ${TEMPLATES}/dashboard/patch-token-ttl.yaml)" -n kube-system

.PHONY: delete-dashboard
delete-dashboard: kill-ui ## Delete dashboard
	@kubectl delete --kubeconfig=$$HOME/.kube/${CLUSTER_NAME} -f ${KUBERNETES_DASHBOARD} --ignore-not-found

.PHONY: ui
ui: dashboard ## kubectl proxy and open dashboard ui
	@${SCRIPTS_DIR}/kill-port.sh 8001; kubectl --kubeconfig=$$HOME/.kube/${CLUSTER_NAME} proxy -p 8001 &> /dev/null &
	@while ! kubectl get pods -n kube-system -o json -l k8s-app=kubernetes-dashboard | jq .items'[]'.status.containerStatuses'[]'.ready | grep -q true; \
	do \
		echo Waitting for UI ; \
		sleep 10 ; \
	done
	@echo
	@echo To access endpoint of dashboard local proxy, go to:
	@echo http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
	@echo
	@echo "Please 'make kill-ui' to close kube proxy connection"

.PHONY: admin-token
admin-token: TOKEN_NAME=$$(kubectl get sa -n kube-system ${SA_NAME} -o json | jq -r '.secrets[].name')
admin-token: ## Get admin-token to login to dashboard
	kubectl describe secret -n kube-system ${TOKEN_NAME} | grep ^token:

########################
## Tiller agent for helm
#########################
.PHONY: tiller
tiller: ## install/upgrade helm tiller
	@-kubectl create serviceaccount --namespace kube-system tiller || true
	@-kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
	#kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
	helm init --service-account tiller --upgrade --tiller-image ${TILLER_IMAGE}
.PHONY: destroy-tiller
destroy-tiller: ## destroy helm tiller
	kubectl delete serviceaccount --namespace kube-system tiller
	kubectl delete clusterrolebinding tiller-cluster-rule
	kubectl -n "kube-system" delete deployment tiller-deploy

##############################
## Heapster to collect metrics
##############################
.PHONY: heapster
heapster: ## Install heapster
	@if ! helm status heapster > /dev/null 2>&1; \
	then \
		helm install --name heapster --namespace kube-system --set rbac.create=true stable/heapster ; \
	else \
		echo Heapster is installed. ; \
	fi
PHONY: upgrade-heapster
upgrade-heapster: ## Update heapster
	helm upgrade heapster --set rbac.create=true stable/heapster
.PHONY: destroy-heapster
destroy-heapster: ## Destroy heapster
	helm del heapster

###############
## External DNS
###############
.PHONY: external-dns
external-dns: config-kube ## Install external-dns
	@if ! helm status external-dns > /dev/null 2>&1; \
	then \
		cat ${TEMPLATES}/external-dns/config.yaml | envsubst | \
		helm install -f - --name external-dns --namespace kube-system stable/external-dns ; \
	else \
		echo external-dns is installed. ; \
	fi
.PHONY: upgrade-external-dns
upgrade-external-dns: ## Update external-dns
	cat ${TEMPLATES}/config.yaml | envsubst | \
		helm upgrade -f external-dns stable/external-dns
.PHONY: destroy-extenral-dns
destroy-extenral-dns: ## Destroy extenral-dns
	helm del extenral-dns

########################
## Fluentd-cloudwatch
########################
# Use debian image
# See https://github.com/fluent/fluentd-kubernetes-daemonset/issues/170
# We need to fetch the chart first to seed the AWS key and key_id

.PHONY: fetch-fluentd-cloudwatch-chart
fetch-fluentd-cloudwatch-chart: ## Fetch fluentd-cloudwatch-chart
	@mkdir -p ./build
	@helm fetch -d build/ --untar incubator/fluentd-cloudwatch
.PHONY: fluentd-cloudwatch-secret
fluentd-cloudwatch-secret: SECDIR=./build/fluentd-cloudwatch/secrets
fluentd-cloudwatch-secret: fetch-fluentd-cloudwatch-chart ## Create secret files needed by fluentd
	kubectl delete secret fluentd-cloudwatch -n kube-system --ignore-not-found
	@mkdir -p ${TMPDIR}
	vault-get ${SEC_PATH}/iam/aws_iam_access_key_deployment_secret > ${SECDIR}/aws_secret_access_key
	vault-get ${SEC_PATH}/iam/aws_iam_access_key_deployment_id > ${SECDIR}/aws_access_key_id
.PHONY: fluentd-cloudwatch
fluentd-cloudwatch: fluentd-cloudwatch-secret ## Deploy fluentd-cloudwatch
	@helm install --name fluentd-cloudwatch --namespace kube-system -f ${TEMPLATES}/fluentd-cloudwatch/configmap.yml \
		 --set awsRegion=${AWS_REGION} \
		 --set rbac.create=true \
		 --set image.tag=v1.2-debian-cloudwatch \
		./build/fluentd-cloudwatch
	@kubectl get daemonset,pods -n kube-system -l k8s-app=fluentd
	@rm -rf ./build/fluentd-cloudwatch/*

.PHONY: upgrade-fluentd-cloudwatch
upgrade-fluentd-cloudwatch: fluentd-cloudwatch-secret ## Upgrade fluentd-cloudwatch
	helm upgrade --namespace=kube-system  --recreate-pods \
		-f ${TEMPLATES}/fluentd-cloudwatch/configmap.yml \
		--set awsRegion=${AWS_REGION} \
		--set rbac.create=true \
		--set image.tag=v1.2-debian-cloudwatch \
		fluentd-cloudwatch ./build/fluentd-cloudwatch
	@rm -rf ./build/fluentd-cloudwatch/*

.PHONY: delete-fluentd-cloudwatch
delete-fluentd-cloudwatch: ## Delete fluentd-cloudwatch
	helm del --purge fluentd-cloudwatch


#######################
## Prometheus
########################

.PHONY: prometheus
prometheus:  ## Deploy prometheus
	@helm install --name prometheus --namespace svc-monitoring  \
		 --set server.persistentVolume.storageClass=${AWS_DEFAULT_SC} \
		 --set alertmanager.persistentVolume.storageClass=${AWS_DEFAULT_SC} \
		 stable/prometheus

.PHONY: upgrade-prometheus
upgrade-prometheus:  ## Upgrade prometheu
	helm upgrade --recreate-pods \
		--set server.persistentVolume.storageClass=${AWS_DEFAULT_SC} \
		--set alertmanager.persistentVolume.storageClass=${AWS_DEFAULT_SC} \
		prometheus stable/prometheus

.PHONY: prometheus-ui
prometheus-ui: POD_NAME=$$(kubectl get pods --namespace svc-monitoring -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")
prometheus-ui: ## Proxy to prometheus UI
	@${SCRIPTS_DIR}/kill-port.sh 9090; kubectl --namespace svc-monitoring port-forward ${POD_NAME} 9090

.PHONY: delete-prometheus
delete-prometheus : ## Delete prometheus
	helm del --purge prometheus

#######################
## Grafana
########################
.PHONY: grafana
grafana:  ## Deploy grafana
	@cat ${TEMPLATES}/grafana/config.yaml | envsubst | \
	helm install --name grafana --namespace svc-monitoring  -f - \
		 stable/grafana

.PHONY: upgrade-grafana
upgrade-grafana:  ## Upgrade grafana
	@helm upgrade --recreate-pods \
		--set persistence.storageClassName=${AWS_DEFAULT_SC} \
		--set persistend=true \
		grafana stable/grafana

.PHONY: grafana-ui
grafana-ui: POD_NAME=$$(kubectl get pods --namespace svc-monitoring -l "app=grafana" -o jsonpath="{.items[0].metadata.name}")
grafana-ui: ## Proxy to prometheus UI
	@${SCRIPTS_DIR}/kill-port.sh 3000; kubectl --namespace svc-monitoring port-forward ${POD_NAME} 3000

PHONY: grafana-admin
grafana-admin: ## Get grafana admin pass
	@kubectl get secret --namespace svc-monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

.PHONY: delete-grafana
delete-grafana : ## Delete grafana
	helm del --purge grafana


##############################
## nginx-ingress
##############################
.PHONY: nginx-ingress
nginx-ingress: delete-nginx-ingress ## Install RBAC nginx-ingress
	@# https://github.com/kubernetes/ingress/tree/master/examples/rbac/nginx
	cat ${TEMPLATES}/nginx-ingress/config.yaml | envsubst | \
		helm install -f - --name nginx-ingress --namespace kube-system \
		--set controller.service.loadBalancerSourceRanges={${MY_IP}} \
		stable/nginx-ingress

.PHONY: upgrade-nginx-ingress
upgrade-nginx-ingress: ## Update nginx-ingress controller
	helm upgrade ${HELM_RELEASE_PREFIX}-nginx-ingress --namespace kube-system \
		-f nginx-ingress/config.yaml \
		--set controller.service.loadBalancerSourceRanges={${MY_IP}} \
		stable/nginx-ingress

.PHONY: delete-nginx-ingress
delete-nginx-ingress: ## Delete nginx-ingress
	@-helm delete --purge ${HELM_RELEASE_PREFIX}-nginx-Ingress
