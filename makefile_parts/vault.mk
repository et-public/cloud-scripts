# Vault operations

.PHONY: vault-unseal
vault-unseal: check-tools ## Unseal Vault
	@${SCRIPTS_DIR}/vault-unseal.sh \
		-s ${VAULT_KEY_SHARES} \
		-v vault0.${ROUTE53_ZONE_NAME}
.PHONY: vault-seal
vault-seal: check-tools ## Seal Vault in emergency
	@${SCRIPTS_DIR}/vault-seal.sh

.PHONY: vault-init
vault-init: check-tools ## Initialize vault. Operations need to be on VPN.
	${SCRIPTS_DIR}/vault-init.sh \
		-s ${VAULT_KEY_SHARES} -t ${VAULT_KEY_THRESHOLD} \
		-u ${VAULT_UNSEALERS} \
		-v vault0.${ROUTE53_ZONE_NAME}

.PHONY: vault-status
vault-status: ## Check vault servers status
	@${SCRIPTS_DIR}/vault-status.sh

.PHONY: vault-login
vault-login:  ## vault login with default ldap backend
	@${SCRIPTS_DIR}/vault-login.sh

.PHONY: vault-login-token
vault-login-token:  ## vault login with token backend
	@export VAULT_AUTH_METHOD="token"; \
	${SCRIPTS_DIR}/vault-login.sh

.PHONY: vault-show-mounts
vault-show-mounts: vault-login ## list vault mounts
	@${SCRIPTS_DIR}/vault-show-mounts.sh

vault-show-auth:  ## list vault auth backend
	@${SCRIPTS_DIR}/vault-show-auth.sh

.PHONY: vault-logout
vault-logout:  ## vault logout
	@${SCRIPTS_DIR}/vault-logout.sh
