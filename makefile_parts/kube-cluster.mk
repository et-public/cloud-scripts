

################################################################################
## kube-cluster.mk
# Makefile targets useful for kube cluster ops
################################################################################

KUBECONFIG := ${HOME}/.kube/${CLUSTER_NAME}
AWS_PROFILE := ${AWS_PROFILE}
ifndef CLUSTER_NAME
	missing_vars := ${missing_vars} CLUSTER_NAME
endif

ifndef AWS_PROFILE
	missing_vars := ${missing_vars} AWS_PROFILE
endif

ifndef TILLER_IMAGE
	TILLER_IMAGE := gcr.io/kubernetes-helm/tiller:v2.9.1
endif

.PHONY: my-token
my-token: ## Get my AWS IAM token to login to Kubernetes
	@aws-iam-authenticator -i ${CLUSTER_NAME} token  | jq -r '.status.token'
	
.PHONY: describe-cluster
describe-cluster: ## Check EKS cluster settings
	aws eks describe-cluster --name ${CLUSTER_NAME} --query cluster
list-clusters: ## show all EKS clusters
	@aws eks list-clusters

.PHONY: cluster-info
cluster-info: ## show EKS cluster info
	@kubectl cluster-info
	@kubectl get all --all-namespaces

.PHONY: get-images
get-images: ## List all images running in the cluster
	@kubectl get pods --all-namespaces -o jsonpath="{..image}" | tr -s '[[:space:]]' '\n' | sort | uniq -c

.PHONY: check-images ## check all running images
check-images: ## check all running images
	kube-images-report.sh
.PHONY: cordon
cordon: NODES = $$(kubectl get node -o json  | jq -r '.items[].metadata.name')
cordon: ## Disable scheduling on a node
	kubectl get nodes
	@for i in ${NODES}; do \
		echo "" ; \
		echo "Cordon $$i?" ; \
		confirm 2> /dev/null || continue ; \
		kubectl cordon $$i ; \
		kubectl drain $$i --ignore-daemonsets --delete-local-data ; \
	done

.PHONY: uncordon
uncordon: NODES = $(shell kubectl get node -o json  | jq -r '.items[].metadata.name')
uncordon: ## Enable scheduling on a node
	kubectl get nodes
	@for i in ${NODES}; do \
		echo "" ; \
		echo "Uncordon $$i?" ; \
		confirm 2> /dev/null || continue ; \
		kubectl uncordon $$i ; \
	done

.PHONY: recycle-node
recycle-node: NODES = $$(kubectl get node -o json  | jq -r '.items[].metadata.name')
recycle-node: ## Replace node with a new instance
	kubectl get nodes
	@for i in ${NODES}; do \
		echo "" ; \
		echo "Recycle $$i?" ; \
		confirm 2> /dev/null || continue ; \
		instance_id=`kubectl get node $$i -o json | jq -r '.spec.externalID'` ; \
		aws autoscaling terminate-instance-in-auto-scaling-group  \
		  --instance-id $$instance_id --no-should-decrement-desired-capacity ; \
	done

.PHONY: reboot-node
reboot-node: NODES = $$(kubectl get node -o json  | jq -r '.items[].metadata.name')
reboot-node: ## Replace node with a new instance
	kubectl get nodes
	@for i in ${NODES}; do \
		echo "" ; \
		echo "Reboot $$i?" ; \
		confirm 2> /dev/null || continue ; \
		instance_id=`kubectl get node $$i -o json | jq -r '.spec.externalID'` ; \
		aws ec2 reboot-instances  \
		  --instance-id $$instance_id ; \
	done
