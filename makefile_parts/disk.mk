

################################################################################
## disk.mk
# Makefile goals for persistent disk/volume management
################################################################################

ifndef DATA_DISK_SIZE
	missing_vars := ${missing_vars} DATA_DISK_SIZE
endif

ifndef STORAGE_CLASS_ZONE
	missing_vars := ${missing_vars} STORAGE_CLASS_ZONE
endif

ifndef STORAGE_CLASS_TYPE
	missing_vars := ${missing_vars} TORAGE_CLASS_TYPE STORAGE_CLASS_NAME
endif

### Storge class
.PHONY: create-storage-class
create-storage-class: ## Create storage class
	@cat ${TEMPLATES}/storageclass.yml | envsubst | kubectl apply -f -
.PHONY: get-storage-class
get-storage-class: ## Get defined storage class
	@kubectl get sc
PHONY: destroy-storage-class
destroy-storage-class: ## Destroy storage class
	@cat ${TEMPLATES}/storageclass.yml | envsubst | kubectl delete -f -

#### PVC disks
PHONY: create-disk
create-disks: storage-class ## Create persistent disks
	@cat ${TEMPLATES}/pvc.yml | envsubst | kubectl apply -f -
.PHONY: get-disks
get-disks: ## Describe pv,pvc
	kubectl get pv,pvc -n ${APP_NAMESPACE}
.PHONY: destroy-disks
distroy-disks: ## Deistroy persistent disks
	@cat ${TEMPLATES}/pvc.yml | envsubst | kubectl delete -f -
	@$(MAKE) destroy-storage-class

## end of disk.mk
