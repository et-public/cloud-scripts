

################################################################################
## vault.mk
# Targets for interacting with vault
################################################################################

ifndef VAULT_ADDR
	missing_vars := ${missing_vars} VAULT_ADDR
endif

# OPTIONAL: VAULT_AUTH_PATH, VAULT_AUTH_METHOD

# target for checking if this file is included
vault-mk:
	true

vault-status: ## Check vault servers status
	@${SCRIPTS_DIR}/vault-status.sh

check-vault:
	@if ! type vault > /dev/null 2>&1; then \
		>&2 echo 'ERROR: Can not find vault binary! Run `make deps` (if available in this project), or else install from https://vaultproject.io'; \
		exit 1; \
	fi

# Targets for vault ops
.PHONY: vault-info
vault-info:  ## show vault info
	@${SCRIPTS_DIR}/vault-info.sh

.PHONY: vault-login
vault-login: check-vault ## vault login
	@${SCRIPTS_DIR}/vault-login.sh

.PHONY: vault-logout
vault-logout:  ## vault logout
	@${SCRIPTS_DIR}/vault-logout.sh
