################################################################################
# cert-manager.mk
# See https://github.com/jetstack/cert-manager/blob/master/docs/user-guides/deploying.md
# See https://github.com/kubernetes/charts/blob/master/stable/cert-manager/README.md
################################################################################

ifndef ACME_URL
	export ACME_URL := https://acme-staging.api.letsencrypt.org/directory
endif

ifndef CERT_MANAGER_NS
	export CERT_MANAGER_NS := kube-system
endif

# NOTE cert-manager is not currently support set up renew-before-days, but the defualt is 30 days.
# See https://github.com/jetstack/cert-manager/blob/ce9e5ede2bc6d8ccbc8e4db086f84c17e3f4d3af/docs/user-guides/acme-http-validation.md
ifndef CERT_RENEW_BEFORE_DAYS
	export CERT_RENEW_BEFORE_DAYS := 30
endif

# TEMPLATES is the parent dir of the cert-manager/*.yaml files
ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
else
	CERT_TEMPLATES := ${TEMPLATES}/cert-manager
endif

.PHONY: create-cert-manager-ns
cert-manager-ns: config-kube ## create cert-manager namespace
	@if ! kubectl get namespace ${CERT_MANAGER_NS} &> /dev/null ; then \
		kubectl create namespace ${CERT_MANAGER_NS} ; \
	fi

# See Tiller issue on create RBAC: https://github.com/jetstack/cert-manager/issues/256
.PHONY: deploy-cert-manager
deploy-cert-manager: cert-manager-ns ## create the cert-manager
	@helm install \
    	--name cert-manager \
    	--namespace ${CERT_MANAGER_NS} \
		--set rbac.create=false \
    	stable/cert-manager || $(MAKE) deploy-cert-manager-wo-helm

.PHONY: deploy-cert-manager-wo-helm
deploy-cert-manager-wo-helm: CERT_CRD_RESOURCES := 00-namespace.yaml serviceaccount.yaml certificate-crd.yaml clusterissuer-crd.yaml deployment.yaml issuer-crd.yaml rbac.yaml
deploy-cert-manager-wo-helm:  ## Deploy cert manager, without helm
	@for i in ${CERT_CRD_RESOURCES}; do \
		kubectl apply -f ${CERT_TEMPLATES}/$$i ; \
	done

.PHONY: ls-cert-manager
ls-cert-manager: ## list the cert-manager
	@helm ls --all cert-manager

.PHONY: cert-manager-status
cert-manager-status: ## check cert-manager status
	@helm status cert-manager || kubectl describe deployment cert-manager -n ${CERT_MANAGER_NS}

.PHONY: update-cert-manager
update-cert-manager: config-kube ## Update the cert-manager deployment
update-cert-manager: CERT_CRD_RESOURCES := deployment.yaml
update-cert-manager: config-kube
	@for i in ${CERT_CRD_RESOURCES}; do \
		echo $$i ; \
		kubectl apply -f ${CERT_TEMPLATES}/$$i ; \
	done

.PHONY: destroy-cert-manager
destroy-cert-manager: config-kube ## destroy the cert-manager 
	@helm delete --purge cert-manager || $(MAKE) destroy-cert-manager-wo-helm
.PHONY: destroy-cert-manager-wo-helm
destroy-cert-manager-wo-helm: CERT_CRD_RESOURCES := certificate-crd.yaml clusterissuer-crd.yaml deployment.yaml issuer-crd.yaml rbac.yaml serviceaccount.yaml 00-namespace.yaml 
destroy-cert-manager-wo-helm: config-kube ## destroy the cert-manager
	@for i in ${CERT_CRD_RESOURCES}; do \
		echo $$i ; \
		kubectl delete -f ${CERT_TEMPLATES}/$$i ; \
	done
	
.PHONY: deploy-issuers
deploy-issuers: KUBE_SEC_DEF_FILE = ${CERT_TEMPLATES}/secret.yaml
deploy-issuers: kube-sec ## create cert issuers
	@kube_apply.sh ${CERT_TEMPLATES}/cert-issuers.yaml

.PHONY: destroy-issuer
destroy-issuers: destroy-kube-sec ## destroy cert issuers
	@kube_delete.sh ${CERT_TEMPLATES}/cert-issuers.yaml

.PHONY: show-issuers
show-issuers: ## show cert issuers
	@kubectl get clusterissuer,issuer --all-namespaces

## end of cert-manager.mk
