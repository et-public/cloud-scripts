#### Functions

# Error handling
err() {
  echo
  echo ${red}ERROR: ${@}$reset
  echo
  exit 1
}

err_report() {
  echo "$1: error on line $2"
}

trap_errors() {
  if [ "$debug_scripts" = "true" ]; then
    set -x
  fi

  set -eeuo pipefail
  trap 'err_report $BASH_SOURCE $LINENO' err
  export shellopts
}

# Confirm action
ask_me(){
  echo $1
  echo "CONTINUE? [Y/N]: "; read ANSWER
	if [ ! "$ANSWER" = "Y" ]; then
		abort 1 "Do nothing Exiting." 
	fi 
}

# display error message and return with return code rc
abort() {
  rc=$1; shift
  echo "$*" && exit $rc
}

try() { "$@" || abort 1 "cannot $*"; }

check_vault_cmd() {
  if ! which vault > /dev/null; then
    abort 1 "ERROR: The vault executable was not found. This script requires vault."
  fi
}

check_bucket() {
  if ! aws s3 ls s3://$s3_bucket > /dev/null ; then
    abort 1 "ERROR: $s3_bucket doesn't exist?"
  fi
}

clone_public_keys() {
  echo "git -C $TMPDIR clone $GPGKEY_REPO"
  if ! git -C $TMPDIR clone $GPGKEY_REPO > /dev/null 2>&1 ; then
    abort 1 "ERROR: Cannot clone $GPGKEY_REPO."
  else
    (cd ${UNSEAL_KEYS_DIR}; echo "git-crypt-unlock"; git-crypt unlock)
  fi
}

# This is the intial setup. Future re-keys need to be done manually.
update_unsealer_keys() {
  cd ${UNSEAL_KEYS_DIR}
  git add .
  if ! git commit -m "Update unseal keys" && git push ; then
    abort 1 "ERROR: Cannot commit unsealer keys to $GPGKEY_REPO."
  fi
  if ! git push ; then
    abort 1 "ERROR: Cannot push unsealer keys to $GPGKEY_REPO."
  fi
}

vault_show_mounts() {
  vault secrets list
}
vault_show_auth() {
  vault auth list
}

# This is dangerous. Do it manually if needed.
vault_reset() {
  ask_me "Reset vault data storage. ${AWS_ACCOUNT}-${ENVIRONMENT}-vault-s3-backend will be destroyed. Data cannobe be recovered."
  aws --profile ${AWS_PROFILE} s3 \
		rm s3://${AWS_ACCOUNT}-${ENVIRONMENT}-vault-s3-backend/core --recursive
	aws --profile ${AWS_PROFILE} s3 \
		rm s3://${AWS_ACCOUNT}-${ENVIRONMENT}-vault-s3-backend/sys --recursive
	aws --profile ${AWS_PROFILE} s3 \
		rm s3://${AWS_ACCOUNT}-${ENVIRONMENT}-vault-s3-backend/unseal-tokens --recursive
  abort 0 "Each Vault server needs to be restarted for the reset to take effet."
}

vault_init() {
  vault operator init -status
  # Return code of 0: Vault is initialized, 2: Vault is not nitialized 1: error
  rc=$?
  if [[ $rc -eq 0 ]]; then
    abort 0 "Vault is already initialized."
  elif [[ $rc -eq 1 ]]; then
    abort 1 "Vault is not ready to be initialized. Please try again."
  fi
  echo "Initialize Vault with PGPs from $vault_unseal_users"
  clone_public_keys
  ( cd $UNSEAL_KEYS_DIR/pubkeys
    vault operator init -key-shares=$key_shares \
      -key-threshold=$key_threshold \
      -pgp-keys=$vault_unseal_users \
      | tee $TMPDIR/vault.init > /dev/null
  )

  if ! cat $TMPDIR/vault.init | grep '\(Unseal Key\)' ; then
    abort 1 "Vault init failed."
  fi
  IFS=', ' read -r -a unsealers <<< "$vault_unseal_users"
  echo "Store public key encrypted unseal keys in git for operator to retrieve."
  COUNTER=1
  cat $TMPDIR/vault.init | grep '\(Unseal Key\)' | awk '{print $4}' | for key in $(cat -); do
    pgp_key=unseal-key-$COUNTER.${unsealers[$((COUNTER - 1))]}
    echo $key > $TMPDIR/$pgp_key
    #aws s3 cp $TMPDIR/$pgp_key s3://$s3_bucket/unseal-tokens/$pgp_key > /dev/null 2>&1
    cp $TMPDIR/$pgp_key $UNSEAL_KEYS_DIR/${ENVIRONMENT}
    COUNTER=$((COUNTER + 1))
  done

  # Push back to git repo
  update_unsealer_keys

  echo "Here is initial root-token:"
  cat $TMPDIR/vault.init | grep '^Initial' | awk '{print $4}'
  #shred -u $TMPDIR/unseal-tokens/*
  #rm -rf $TMPDIR
  instructions
}

vault_status(){
  for i in ${VAULT_SERVERS}
  do
    echo Checking https://$i
    curl -s https://$i/v1/sys/seal-status | jq -r '.'
    echo ""
  done
  exit 0
}

vault_unseal() {
  vault operator init -status
  # Return code of 0: Vault is initialized, 2: Vault is not nitialized 1: error
  if [[ $? -ne 0 ]]; then
    abort 1 "Vault init check failed. Is it not initialized?"
  fi
  echo "Downloading pgp keys."
  clone_public_keys
  #aws s3 cp s3://$s3_bucket/unseal-tokens/ $TMPDIR/unseal-tokens --recursive > /dev/null 2>&1
  if ! gpg --list-secret-keys | grep -q $USER; then
    abort 1 "no secret key found for $USER"
  fi

  found_key=0
  for i in `seq $key_shares`
  do
    if [ -e "${UNSEAL_KEYS_DIR}/vault-${PROVISION_ENV}/unseal-key-$i.$USER" ]; then
      found_key=1
      break
    fi
  done
  if [ $found_key -eq 0 ]; then
    abort 1 "no encrypted unseal key found for $USER"
  fi

  for i in ${VAULT_SERVERS}
  do
    echo "Unseal $i with $USER's key"
    for j in ${UNSEAL_KEYS_DIR}/vault-${PROVISION_ENV}/unseal-key-*.$USER
    do
      if [ -s $j ]
      then
        unseal_key=$( cat $j | base64 -D | gpg -dq )
        VAULT_ADDR=https://$i vault operator unseal $unseal_key
      fi
    done
    echo ""
  done
}

vault_seal() {
  echo "Seal vault. No data will be available."
  echo "Sealing Vault"
  for i in ${VAULT_SERVERS}
  do
    VAULT_ADDR=https://$i vault operator seal
  done
}

instructions() {
  cat <<EOF

The Vault has been automatically initialized and unsealed once. Future unsealing must
be done manually.

This is the only time you can download this root token. Use this to setup up other
type of authentication backend, policies etc, then revoke the root token.

EOF
}
