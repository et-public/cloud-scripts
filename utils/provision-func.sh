bash_defaults() {
  set -eu -o pipefail
  shopt -s nullglob
  shopt -s extglob
  trap 'echo "`basename $0`: error on line $LINENO"' ERR
  export shellopts

  if is_set DEBUG && [ "$DEBUG" = "true" ]; then
    set -x
  fi
}

# inspired by https://github.com/hashicorp/vault-provision-example/blob/master/scripts/provision.sh

# HELPERS
is_set() {
  declare -p $1 &> /dev/null
}

curl_vault() {
  curl --fail -s -L -H "X-Vault-Token: ${VAULT_TOKEN}" "$@" || {
    set -x
    curl -s -L -H "X-Vault-Token: ${VAULT_TOKEN}" "$@"
    false
  }
}

# like curl_vault, but errors aren't displayed
curl_vault_s() {
  curl --fail -s -L -H "X-Vault-Token: ${VAULT_TOKEN}" "$@"
}

# CHECK FUNCTIONS
check_initialized() {
  local path=$1
  curl_vault_s $path | jq -e '.initialized == true' > /dev/null 2>&1
}

list_dirs() {
  path=$1
  curl_vault -X LIST $path | jq -er '.data.keys[]' | grep '/'
}

# take last segment of path, and check if that string exists in parent path
# e.g. for sys/auth/ldap, check if "ldap/" key exists in sys/auth
check_parent_list() {
  local path=$1
  local parent_path=${path%/*}
  local search_key="${path##*/}/"
  curl_vault_s $parent_path | jq -e ".\"${search_key}\"" > /dev/null
}

# use to make provision to always update
always_update() {
  true
}

# do a get on $path to see if resource exists
check_item() {
  local path=$1
  curl_vault_s $path > /dev/null
}

provision_user() {
  user_file=$1
  user_dir=$( dirname $1 )
  username=$(jq -er .username < $user_file)
  user_pass_file=$HOME/.vault_userpass.${VAULT_ADDR#https://}.$username

  # create user with random password if doesn't exist
  pgp_key_file=${user_dir}/_pgp_key.asc
  email=$(gpg --list-packets $pgp_key_file | perl -ne 'print "$1 " if /<(.*)>/' | cut -d ' ' -f1 )
  if ! curl_vault_s $VAULT_ADDR/v1/auth/userpass/users/$username > /dev/null; then
    # export password to be used in JSON template
    export password=$(pwgen 12 | tr -d '\n')
    gpg --import $pgp_key_file > /dev/null 2>&1
    provision -u do_nothing -p auth/userpass/users $user_file
    encrypted_pass=$(echo -n $password | gpg --encrypt --recipient $email | base64 )
    echo $encrypted_pass > $user_pass_file
  else
    echo
    echo "$username is already provisioned."
  fi
  echo "${email} PGP-ENCRYPTED PASSWORD was/is saved at $user_pass_file"

  # assign policies
  provision -c always_update $user_dir/policies.json
}

post_file() {
  json_file=$1
  addr=$2
  jq -rM . < $json_file | envsubst | curl_vault -d @- $addr
}

put_file() {
  json_file=$1
  addr=$2
  jq -rM . < $json_file | envsubst | curl_vault -X PUT -d @- $addr
}

# special case for policies, which have JSON in JSON
put_policy() {
  policy_file=$1
  addr=$2
  jq --arg policy "$(cat $policy_file)" -nrM '{"policy": $policy}' | envsubst | curl_vault -X PUT -d @- $addr
}

do_nothing() {
  true
}

# function to create an LDAP filter that only pulls in necessary groups
generate_ldap_group_filter() {
  group_mappers_path=$1
  filter=
  for mapper in $group_mappers_path/*; do
    filter="${filter}(cn=$(basename $mapper .json))"
  done
  filter="(|${filter})"
  echo $filter
}

# function to output a project subpath viewer policy from a template
generate_project_subpath_policy() {
  export policy_template=$1
  export project_category=$2
  export project=$3
  export project_subpath=$4

  cat $policy_template | envsubst
}

generate_project_subpath_policy_name() {
  export policy_name_template=$1
  export project=$2
  export project_subpath=$3

  cat $policy_name_template | envsubst | jq -er .filename
}

# function to output a project viewer policy from a template
generate_project_policy() {
  export policy_template=$1
  export project_category=$2
  export project=$3

  cat $policy_template | envsubst
}

generate_project_policy_name() {
  export policy_name_template=$1
  export project=$2

  cat $policy_name_template | envsubst | jq -er .filename
}

generate_token_role() {
  export token_role_template=$1
  export role_name=$2
  export allowed_policies=$3

  cat $token_role_template | envsubst
}

generate_approle() {
  export approle_template=$1
  export approle_policy=$2

  cat $approle_template | envsubst
}

# PROVISION FUNCTION
# provision a resource. check if exists, if not create it, optionally update it
provision() {

  # process arguments
  function provision_usage {

	cat <<EOM
Usage: $(basename "$0") [ -c <CHECK_FUNCTION> ] [ -C <CREATE_FUNCTION> ] [ -u <UPDATE_FUNCTION> ] [ -p <API_PATH> ] RESOURCE_PATHS

  -c  CHECK_FUNCTION               function which returns true if an update should happen
  -C  CREATE_FUNCTION              function to create resource (default is post_file)
  -u  UPDATE_FUNCTION              function to update resource (default is put_file)
  -p  API_PATH                     specify relative API endpoint e.g. sys/policy/my_policy (instead of inferring from file location)
EOM

  	exit 2
  }

  # defaults
  local check_function=check_item
  local create_function=post_file
  local update_function=put_file
  local api_path=

  # get function optional args
  # https://stackoverflow.com/a/16655341
  local OPTIND o a
  while getopts ":c:C:u:p:" o; do
    case "${o}" in
      c)
        local check_function="${OPTARG}"
        ;;
      C)
        local create_function="${OPTARG}"
        ;;
      u)
        local update_function="${OPTARG}"
        ;;
      p)
        # specify an API endpoint, other than what is inferred from file location
        # e.g. sys/policy-generated applies to sys/policy. For KV 2, if secret/ path is used, inserat data prefix.
        local api_path=${OPTARG/secret\//secret\/data\/}
        ;;
      *)
        provision_usage
        ;;
    esac
  done
  shift $((OPTIND-1))

  # either a path to a json file, or a directory
  #
  resources="$@"

  for f in $resources; do
    local path="${f%.*}"
    if ! [ -z "$api_path" ]; then
      # we replace the dirname (e.g. sys/policy-generated) part of the path with api_path (e.g. sys/policy)
      # so path sys/policy-generated/policy.hcl becomes sys/policy/policy.hcl
      path="${api_path%/}/${path##*/}"
    fi

    local addr="${VAULT_ADDR}/v1/${path}"
    if $check_function $addr; then
      echo "Updating $path"
      $update_function $f $addr
    else
      echo "Creating $path"
      $create_function $f $addr
    fi
  done
}
