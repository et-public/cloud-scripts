#!/bin/bash

###############################################################################
# apply resource definitions from list of input templates
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PATH="${THIS_DIR}:${PATH}"

# include functions
source $THIS_DIR/functions.sh
source env.sh

# fail on error or undeclared vars
trap_errors

# optional vars
set +u
debug_gomplate=$debug_gomplate
set -u

template_files=$@

echo applying kubernetes resources with templates:
for template in $template_files; do
  echo "   $template"
  if [[ $template == *'.gomplate.'* ]]; then
    if [ "$debug_gomplate" = "true" ]; then
      gomplate.sh < $template
    else
      gomplate.sh < $template | kubectl apply -f -
    fi
  else
    cat $template | envsubst | kubectl apply -f -
  fi
done
