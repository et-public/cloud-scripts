#!/bin/bash
# Usage: input: 2018-03-19T17:42:23.000Z
input=$1
tmpstr=$(echo $input | grep -oE "\d+-\d+-\d+T\d+:\d+.\d+.000Z")
if [ ! -z "$tmpstr" ]; then
  dstring=${tmpstr/.000Z/ +0000}
  local_time=$(date -jf "%Y-%m-%dT%H:%M:%S %z" "$dstring" "+%Y-%m-%d %H:%M.%S")
  echo $local_time
else
  echo Unsupported $input: Please convert to yyyy-mm-ddTHH:MM:ss.000Z format.
fi
