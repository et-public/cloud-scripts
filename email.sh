#!/bin/bash

while getopts ":f:t:r:s:m:" OPTION
do
    case $OPTION in
        f)
          FROM=$OPTARG
          ;;
        t)
          TO=$OPTARG
          ;;
        s)
          SUBJECT=$OPTARG
          ;;
        m)
          MESSAGE=$OPTARG
          ;;
        r)
          AWS_REGION=$OPTARG
          ;;
        ?)
          echo "$0 -r <aws_region> -f <from> -t <rcpt1@example.com,rcpt2@example.com> -s <subject> -m </path/to/message-file>"
          exit
          ;;
    esac
done

if [[ ! $FROM || ! $TO || ! $SUBJECT || ! -f $MESSAGE || ! $AWS_REGION ]]; then
    echo "FROM, TO, SUBJECT or message file, or AWS_REGION  are missing."
     echo "$0 -r <aws-region> -f <from> -t \"<rcpt1@example.com,rcpt2@example.com>\" -s \"<subject>\" -m </path/to/message-file>"
    exit 1
fi

docker run -e FROM="$FROM" -e TO="$TO" -e SUBJECT="$SUBJECT" \
	-e AWS_DEFAULT_REGION="$AWS_REGION" \
	-v $MESSAGE:/tmp/message.txt -v /root/.aws:/root/.aws suet/aws-ses
