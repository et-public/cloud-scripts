#!/bin/bash
set -e
# References: 
# http://cuddletech.com/?p=959
# https://www.digitalocean.com/company/blog/vault-and-kubernetes/

# Create a CA

if [[ $# -lt 1 ]]; then 
    echo "at least 1 argument is required. "
    echo "example - create root ca:"
    echo "${0##*/} root_ca_mount root_ca_cn"
    echo "example - create intermediate ca:"
    echo "${0##*/} -i intermediate_ca_mount intermediate_ca_cn root_ca_mount"
fi
 
# Default to create root ca
interm_ca="false"

# Default CA valid until
rootCAttl="87600h"
intermCAttl="26280h"
keyBits=4096

while getopts ":i:h" OPTION
do
  case $OPTION in
    i)
	interm_ca="true"
        shift
 	;;
    *)
        usage "${0##*/} [-i ] my_ca my_ca_cn"
        
        ;;
   esac
done

create_root_ca() {
    # Generate root ca
    vault write $pki_path/root/generate/internal \
        common_name="$common_name" ttl=$rootCAttl key_bits=$keyBits exclude_cn_from_sans=true

    # Create CA url and CRL
    vault write $pki_path/config/urls \
        issuing_certificates="$VAULT_ADDR/v1/$pki_path/ca" \
        crl_distribution_points="$VAULT_ADDR/v1/$pki_path/crl"

    echo "The $pki_path CA is ready."
}

create_interm_ca() {

    # create csr
    vault write -format=json $pki_path/intermediate/generate/internal \
        common_name="$common_name" ttl=$intermCAttl key_bits=$keyBits exclude_cn_from_sans=true \
        | jq -r '.data.csr' > /tmp/$pki_path.csr

    # Sign the cert with root ca
    vault write -format=json $root_ca_path/root/sign-intermediate csr=@/tmp/$pki_path.csr \
        common_name="$common_name" \
        format=pem_bundle \
        ttl=$intermCAttl | jq -r '.data.certificate' > /tmp/$pki_path.crt
 
     # clean up pem to workaround https://github.com/hashicorp/vault/pull/4148
     sed -i .bak '$d' /tmp/$pki_path.crt

    # Import back to the intermediate CA backend
    vault write $pki_path/intermediate/set-signed certificate=@/tmp/$pki_path.crt

    # Create Intermediate CA url and CRL
    vault write -format=json $pki_path/config/urls \
        issuing_certificates="$VAULT_ADDR/v1/$pki_path/ca" \
        crl_distribution_points="$VAULT_ADDR/v1/$pki_path/crl"

    echo "The Intermediate CA is ready!"
}

## MAIN

# PKI mount path
pki_path=$1
common_name=$2
root_ca_path=$3

if [[ -z "$pki_path" ]] || [[ -z $common_name ]]; then
  echo "pki_mount_path and common_name are required, eg:"
  echo "${0##*/} my_pki 'My CA Root Authority'"
  exit 1
fi


if [[ "$interm_ca" = "true" ]] && [[ -z $3 ]]; then
  echo "you must specify a root ca in order to sign the cert of the intermediate CA, eg:"
  echo "${0##*/} -i intermediate_ca_mount intermediate_ca_cn root_ca_mount"
  exit 1
elif [ "$interm_ca" = "true" ]; then
  #description="$description Intermediate CA"
  ttl=$intermCAttl
else
  #description="$description Root CA"
  ttl=$rootCAttl
fi

# Mount pki path
if vault secrets list | grep ^$pki_path | grep pki; then
    echo "$pki_path PKI backend already mounted. Skipping re-mount"
else
    vault secrets enable -path=$pki_path -description="$description" -max-lease-ttl=$ttl pki
    vault secrets tune -max-lease-ttl=87600h $pki_path
fi

# Check if CA already exist.
if curl -s $VAULT_ADDR/v1/$pki_path/ca/pem \
    | openssl x509 -text > /dev/null 2>&1; then
    echo "$pki_path CA exists. Skipping."
elif [ $interm_ca = "true" ]; then
    create_interm_ca
else
    create_root_ca
fi

