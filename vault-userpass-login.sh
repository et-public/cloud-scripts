#!/bin/bash

###############################################################################
# login to vault with username/passord. Account needs to be provisioned first.
###############################################################################

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# include functions
source $THIS_DIR/utils/functions.sh

trap_errors

set +u # optional vars
export VAULT_ADDR=${VAULT_ADDR:-https://vault.stanford.edu}
export VAULT_SEC_PATH=${VAULT_SEC_PATH:-auth/token/lookup-self}
export VAULT_AUTH_PATH=${VAULT_AUTH_PATH:-userpass}
export VAULT_AUTH_METHOD=${VAULT_AUTH_METHOD:-userpass}
export VAULT_USER=${LOGNAME-:-$USER}
export VAULT_USERPASS_FILE=${VAULT_USERPASS_FILE:-$HOME/.vault_userpass.${VAULT_ADDR#https://}.$VAULT_USER }

set -u

echo "VAULT ADDR: $VAULT_ADDR"
unset VAULT_TOKEN
vault-logout

if ! vault token lookup > /dev/null 2>&1; then
    if ! [ -f $VAULT_USERPASS_FILE ]; then
        abort 1 "$VAULT_USERPASS_FILE doesn't exit."
    else 
        password=$( cat $VAULT_USERPASS_FILE | base64 -D | gpg -d )
        echo Please enter decrypted password below: $password
        vault login -method=${VAULT_AUTH_METHOD} -path=${VAULT_AUTH_PATH} username=$VAULT_USER
    fi
fi

# Warning if using root token
if vault token capabilities ${VAULT_SEC_PATH}/* | grep -q root; then
    echo "You are logged in VAULT with root token!"
    exit 0
fi

if vault token capabilities ${VAULT_SEC_PATH}/* | grep -q read; then
    echo "You are logged in VAULT and have permission to read from ${VAULT_SEC_PATH}/*"
else
    echo "Permission denied to read from ${VAULT_SEC_PATH}/*, please login again:"
    vault login -method=${VAULT_AUTH_METHOD} -path=${VAULT_AUTH_PATH}
fi
