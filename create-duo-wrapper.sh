#!/bin/bash -e

# Copy create-duo.sh to cardinal.stanford.edu and run remote shell to generate duo api key.
# Your kerberos ticket is required.

node=$1
template=$2

# Server where you can ssh into and has wallet client installed
remote_server="cardinal.stanford.edu"

# Tmp file
rand=/tmp/duo-$RANDOM

# Check if the duo key already exist in vault
# VENDOR_ACCOUNT is something to identify which account this duo key is used
# e.g. anchorage.
if vault kv get secret/global/${VENDOR_ACCOUNT}/vault/${PROVISION_ENV}/duo/ikey ; then
  echo "Duo key for $node already exists at secret/global/${VENDOR_ACCOUNT}/vault/dev/duo/ikey."
  echo "Skipping create duo key."
  exit 0
fi

if ! klist -t > /dev/null 2>&1 ; then
  echo "Running kinit to get kerberos ticket. Please enter your SUNet ID authentication."
  kinit $USER
fi
if [[ "X$node" = "X" ]]; then
  echo "$(basename $0) new-node-name [template-node-name]"
  exit 1
fi

THIS_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo ""
echo "Copy create-duo.sh to $remote_server, and run remote shell to execute it. You will be asked to autenticate twice."
echo ""
scp -p ${THIS_DIR}/create-duo.sh ${remote_server}:~/create-duo.sh
echo ""
ssh $USER@${remote_server} ~$USER/create-duo.sh $node $template > $rand
		
if grep -q ikey $rand ; then
  vault kv put secret/global/${VENDOR_ACCOUNT}/vault/${PROVISION_ENV}/duo/ikey format=text value=`grep ikey $rand | cut -d ' ' -f3`
  vault kv put secret/global/${VENDOR_ACCOUNT}/vault/${PROVISION_ENV}/duo/skey format=text value=`grep skey $rand | cut -d ' ' -f3`
  vault kv put secret/global/${VENDOR_ACCOUNT}/vault/${PROVISION_ENV}/duo/host format=text value=`grep host $rand | cut -d ' ' -f3`
	rm -rf $rand
else
  echo "No duo keys found in $rand."
  exit 1
fi
