#!/bin/bash
#
# Register with AWS route53 service.
# This is for a particular use case that you want to register a group of instances in an ASG group.
#
# Given a name prefix 'node', instances in ASG  will be registered as node0.example.com, node1.example.com, node2.example.com...
#
# Suport for A (ipv4) record only. Can be easily changed to support other types.
#

if [ $# -ne 5 ];
then
 echo "$0 -u -z <route53_zone_name> -n <node_name_prefix>"
 exit 1
fi

readonly AWS_DEFAULT_REGION=$(curl -s http://instance-data/latest/dynamic/instance-identity/document | jq --raw-output .region)
MY_IP=$(curl -s http://instance-data/latest/meta-data/public-ipv4)

need_update(){
  asgips_tmp=$(mktemp /tmp/asgips_tmp.XXX)
  dns_tmp=$(mktemp /tmp/dns_tmp.XXX)
  count=0
  for i in $ASGIPS
  do
    echo $i >> $asgips_tmp
    dns=$(host ${node_name_prefix}${count}.${zone_name} | awk '{print $4}')
    echo $dns >> $dns_tmp
    let count++
  done

  sort $asgips_tmp > $asgips_tmp.sorted
  sort $dns_tmp > $dns_tmp.sorted
  diff=$(comm -3 $asgips_tmp.sorted $dns_tmp.sorted)
  [[ -z "$diff" ]] && return 1 || return 0;
}

while getopts ":uz:n:" OPTION
do
    case $OPTION in
        u)
          action="UPSERT"
          ;;
        n)
          node_name_prefix=$OPTARG
          ;;
        z)
          zone_name=$OPTARG
          ;;
        ?)
          echo "$0 -u -z <zone> -n <node_name_prefix>"
          exit
          ;;
    esac
done

# AWS cli container image
readonly IMAGE=suet/awscli:latest
docker pull $IMAGE > /dev/null 2>&1

# Private or public zone
# Output from contains is asgname: ip1 ip2 ip3 ..
if [[ $zone_name == *.internal ]] || [[ $zone_name == "*.local" ]] || [[ $zone_name == *.private ]] ;
then
    MY_IP=$(curl -s http://instance-data/latest/meta-data/local-ipv4)
    ASGIPS=$(docker run --rm $IMAGE get-metadata asgipsprv | awk -F':' '{print $2}')
else
    ASGIPS=$(docker run --rm $IMAGE get-metadata asgipspub | awk -F':' '{print $2}')
fi

if echo $ASGIPS | grep -v ${MY_IP} ; then
  echo "${MY_IP} doesn't belong to an ASG."
fi

# Update Route53 record
CMD="aws route53 --region ${AWS_DEFAULT_REGION} list-hosted-zones-by-name --max-items 1 --dns-name $zone_name | grep -Eo 'hostedzone/[[:alnum:]]+' | sed 's#hostedzone/##'"
hosted_zone_id=$(docker run --rm $IMAGE /bin/bash -c "$CMD")

if ! need_update ; then
  exit 0
fi

count=0
for i in $ASGIPS
do
  change_set=$(mktemp /tmp/change-set-$count.XXX)
  cat > $change_set <<CHANGESET
{
    "Comment": "Updated by $0",
    "Changes": [
        {
            "Action": "${action}",
            "ResourceRecordSet": {
                "Name": "${node_name_prefix}${count}.${zone_name}",
                "Type": "A",
                "TTL": 30,
                "ResourceRecords": [
                    {
                        "Value": "${i}"
                    }
                ]
            }
        }
    ]
}
CHANGESET

  # Update the DNS record
  CMD="aws route53 --region ${AWS_DEFAULT_REGION} change-resource-record-sets --hosted-zone-id $hosted_zone_id --change-batch file://$change_set"
  docker run --rm -v $change_set:$change_set $IMAGE /bin/bash -c "$CMD"
  let count++
  echo ${count}
done
