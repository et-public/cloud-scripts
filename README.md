
Shared scripts to automate processes, such AWS infrastructure build, Vault operations, Kubernetes. 

Usage: Usually in your code or manually, checkout the repoisitory in $HOME/bin so it become part of the command search path.

Credits: Majority of the code structure, scripts and make files are adopted from: https://gitlab.med.stanford.edu/irt-public/gcloud-scripts.git.
Many thanks to Christopher Pauley (cmpauley@stanford.edu) and Xuwang (xuwang@stanford.edu) as major contributors. 
