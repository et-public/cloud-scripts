#!/bin/bash
# Adopted from https://gitlab.med.stanford.edu/irt-public/gcloud-scripts/blob/master/vault-write.sh
# Usage:
#   vault-put <path> [<secret vaule> | @<secret file>]

program=$0
path=$1
shift
secret="$*"
if [ -z "$secret" ]
then
  echo "Usage: $0 <path> [<secret vaule> | @<secret file>]"
  exit 1
fi

# Confirm action
ask_me(){
  echo $1
  echo "CONTINUE? [Y/N]: "; read ANSWER
  if [ ! "$ANSWER" = "Y" ]; then
     echo "Do nothing Exiting." && exit 1
  fi
}

if [[ "$secret" =~ ^@ ]];
then
    f=$(echo $secret | cut -c 2-)
    if file -b -I $f | grep -s binary > /dev/null
    then
        v=$(cat $f | base64)
        format=base64
    else
        v=$(cat $f)
        format=text
    fi
else
    v=$secret
    format=text
fi
# Ask to replace existing data
vault kv get "$path" | tee /tmp/$(basename $path).$$
if [[ -f /tmp/$(basename $path).$$ ]] && [[ -s /tmp/$(basename $path).$$ ]] ; then
    ask_me "Replace existing data?"
fi
vault kv put "$path" value="$v" format="$format"
