#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Touch /etc/no-reboot-notification to supress reboot notification.
# This file gets removed once reboot, so it only works once.
if [ -f "/etc/no-reboot-notification" ];
then
  rm /etc/no-reboot-notification
  exit 0
fi

# Get instance ip and instance role
ip=$(curl -s instance-data/latest/meta-data/local-ipv4)
instance_role=$(curl -s instance-data/latest/meta-data/iam/info | jq -r '.InstanceProfileArn' | awk -F'/' '{print $NF}')

FROM=${1:-authnz-team@lists.stanford.edu}
TO=${2:-authnz-alert@alists.stanford.edu}
REGION=${3:-us-west-2}

rm -rf /tmp/reboot-message.txt
cat /etc/os-release | grep PRETTY_NAME | sed 's%"%%g' > /tmp/reboot-message.txt

$DIR/email.sh \
    -f $FROM -t $TO -s "$instance_role at $ip rebooted" \
    -m /tmp/reboot-message.txt -r  $REGION
