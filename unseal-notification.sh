#!/bin/bash

# This unseal watcher should be run as root
# on each vault server.

# Scripts dir
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Get ROUTE53_ZONE_NAME and CLUSTER_NAME
source /etc/profile.d/vault.sh

instructions=/tmp/instructions

help(){
  echo "$0 -r <ses region> -f <from> -t <to>"
}

abort(){
  echo $*
  help
  exit 1
}

# Assumin 3 vault servers, using intenral IP:8200
rm -rf /tmp/vault-status
vault_status(){
  for i in vault{0..2}.${CLUSTER_INTERNAL_ZONE}
  do
    vault status -address=https://$i:8200  -tls-skip-verify >> /tmp/vault-status 2>&1
    rc=$?
    if [ $rc -eq 2 ]
    then
      return 2 
    elif [ $rc -eq 1 ]
    then
      return 1
    fi
  done
  echo 0
}
unseal_instruction() {
  tee "$instructions" > /dev/null <<EOF

One of the vault servers are sealed for ${CLUSTER_NAME}. Please unseal the vault with the following instructions if you haven't done so.

    git clone git@code.stanford.edu:et-public/cloud-scripts.git
    cd cloud-scripts
    ./open-vault.sh ${CLUSTER_NAME} [SUNetID]

Your PGP key passphrase is required to decrypt your unseal key and your SUNetID login password is required to send out email 
notification. 

Because the Vault is configured as a 3-node cluster in HA mode, as long as there is one unsealed server, the vault service is available.

That said, you should still try to unseal as soon as you can. 2 out of 7 unsealers are needed to unseal a vault server. Notification will be sent
out every 10 minutes until vault is unsealed.

If you already run the unseal process, then you can ignore this message.
EOF
}

while getopts ":f:t:r:h" OPTION
do
  case $OPTION in
    f)
      from=$OPTARG
      ;;
    t)
      to=$OPTARG
      ;;
    r)
      aws_region=$OPTARG
      ;;
    *)
      abort
      ;;
  esac
done

if [ "X${from}" = "X" ]; then
  abort "sender email address is required."
fi
if [ "X${to}" = "X" ]; then
  abort "Recipient email address is required."
fi
if [ "X${aws_region}" = "X" ]; then
  abort "AWS REGION for SES endpoint is required."
fi

vault_status
rc=$?
if [ $rc -eq 2 ]
then
  unseal_instruction
  # Send to email
  domain=$(echo $ROUTE53_ZONE_NAME | cut -d'.' -f2-8)
  for i in $(echo $to | sed 's/,/ /g')
  do
    addr=$i
    if ! [[ $i =~ "@" ]]; then
      addr=$i@$domain
    fi
    ${DIR}/email.sh -r $aws_region -f $from -t $addr \
	    -s "Please unseal vault in ${CLUSTER_NAME}" -m $instructions
  done
  exit 0
elif [ $rc -eq 1 ]
then
  echo "" >> /tmp/vault-status
  echo "Maybe caused by a recent reboot. If so, you can ignore." >> /tmp/vault-status
  ${DIR}/email.sh -r $aws_region -f $from -t $from \
	-s "Check unseal status failed in ${CLUSTER_NAME}" -m /tmp/vault-status
  exit 1
fi
exit 0
