#!/bin/bash
export VAULT_ADDR=${VAULT_ADDR:-https://vault.stanford.edu}
unwrap_token=${1:-''}

# Sandity check
is_uuid(){
  [[ $unwrap_token =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]] && return 0 || return 1
}
for t in jq curl
do
   if ! which $t > /dev/null
   then
     echo "ERROR: $$t command not found. Please install it for your platform." && exit 1
   fi
done
if is_uuid $unwrap_token ; then
  curl -k -s --header "X-Vault-Token: $unwrap_token" --request POST https://vault.stanford.edu/v1/sys/wrapping/unwrap  | jq -r '.data'
else
  echo $unwrap_token is not a uuid string.
fi
