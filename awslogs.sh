#!/bin/bash
#
# sfeng@stanford.edu 2018
# 
# Read AWS cloudwatch logs. Default is logs from 5 minutes ago.

usage_example(){
  echo
  echo "Usage examples:"
  echo "$program groups: get all log groups"
  echo "$program get <group name>: get logs for the named log group"
  echo "$program streams <group name>: get all stream names for the given group"
  echo "$program get <group name> <stream name>: get all events  for a group with a stream name."
  echo
  echo "get command has default 5 minutes start time. you can use --since=1{d,m,w} for starting time."
  echo "get command can also use 'ALL --watch' option to tail a log."
  echo "See https://github.com/jorgebastida/awslogs for full usage information."
  exit 0
}

### MAIN
program=$(basename $0 | sed 's/-/_/g')

cmd=${@:-'--help'}

# Make sure docker is installed.
if ! which docker 2>&1 > /dev/null ; then 
  abort 1 "Need to install docker."
fi

aws_profile=${AWS_PROFILE:-"NODEFAULT"}
if [ "$aws_profile" = "NODEFAULT" ]; then
  echo "AWS profile is required. please do: export AWS_PROILE=<profile>."
  exit 1
fi

echo Running $program $cmd --profile=$aws_profile...
if [ "$cmd" = "--help" ] ;then
  docker run --rm -it suet/awslogs $cmd
  usage_example
fi
if docker run --rm -it -v $HOME/.aws:/root/.aws suet/awslogs $* --profile=$aws_profile | grep "An error occurred"; then
  usage_example
else
  docker run --rm -it -v $HOME/.aws:/root/.aws suet/awslogs $* --profile=$aws_profile --timestamp > /tmp/output.$$
fi
cat /tmp/output.$$
echo "File is saved at /tmp/output.$$."
