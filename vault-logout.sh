#!/bin/bash
# https://gitlab.med.stanford.edu/irt-public/gcloud-scripts/blob/master/vault-info.sh
# Revoke login token and all its children
# and cleanup local cache

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export VAULT_ADDR=${VAULT_ADDR:-https://vault.stanford.edu}
VAULT_CACHE=${VAULT_CACHE:-$HOME/.vault-local}

echo "Logout ${VAULT_ADDR}"
vault token-revoke -self &> /dev/null
rm -f ${HOME}/.vault-token

# Cleanup local cache
rm -rf ${VAULT_CACHE}
