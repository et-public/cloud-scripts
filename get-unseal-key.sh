#!/bin/bash -e

# Get unseal key
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

CLUSTER=${1}
USER=${2:-$USER}
if [ -z "$CLUSTER" ]
then
  echo "Vault cluster name is required: [vault-prod|vault-dev]." && exit 1
fi
echo "Your USER environment variable is: $USER."
echo "Continue to get unseal key for $CLUSTER, using $USER's GPG key ? [Y/N]: "
read ANSWER
if [ ! "$ANSWER" = "Y" ]; then \
    echo "Exiting." && exit 1
elif ! gpg --list-secret-keys $USER ; then
    echo "Private key for $USER doesn't exist on this computer." && exit 1
fi

TMPDIR=$(mktemp -d /tmp/vault-unseal.XXXX)
pushd $TMPDIR > /dev/null
git clone git@code.stanford.edu:et/vault-unsealers-team
cd vault-unsealers-team
git-crypt unlock
encrypted_keys=$( ls -1 $CLUSTER/unseal-key-*$USER )
for i in $encrypted_keys
do
  echo "Your unseal key for $CLUSTER is:"
  cat $i | base64 -D | gpg -dq
  echo ""
done

rm -rf $TMPDIR

popd > /dev/null
