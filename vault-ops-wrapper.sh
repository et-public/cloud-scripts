#!/bin/bash
#
# sfeng@stanford.edu 2017
# 
# Require unsealers's gpg key. 

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
CLUSTER=${1:-'vault-prod'}
if [ "$CLUSTER" = "vault-prod" ];
then
    ROUTE53_ZONE_NAME=${ROUTE53_ZONE_NAME:-"vault.stanford.edu"}
else
    ROUTE53_ZONE_NAME=${ROUTE53_ZONE_NAME:-"itlab.stanford.edu"}
fi
VAULT_SERVERS=${VAULT_SERVERS:-"vault0.$ROUTE53_ZONE_NAME vault1.$ROUTE53_ZONE_NAME vault2.$ROUTE53_ZONE_NAME"}
VAULT_SERVERS=$(echo $VAULT_SERVERS | sed "s/:8200//g")
VAULT_SERVERS=$(echo $VAULT_SERVERS | sed "s/$ROUTE53_ZONE_NAME/$ROUTE53_ZONE_NAME:8200/g")

#AWS_PROFILE=${AWS_PROFILE:-NODEFAULT}

. $DIR/utils/env_defaults
. $DIR/utils/functions.sh

### MAIN
program=$(basename $0 .sh | sed 's/-/_/g')

while getopts ":b:i:p:s:t:u::v:h" OPTION
do
  case $OPTION in
    i)
      interactive
      ;;
    b)
      s3_bucket=$OPTARG
      ;;
    s)
      key_shares=$OPTARG
      ;;
    t)
      key_threshold=$OPTARG
      ;;
    u)
      vault_unseal_users=$OPTARG
      ;;
    v)
      # This is used in vault operator init -status check.
      export VAULT_ADDR=https://$OPTARG:8200
      ;;
    *)
      help
      exit 1
      ;;
  esac
done

check_vault_cmd
echo Running $program...
$program
