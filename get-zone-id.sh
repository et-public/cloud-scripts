#!/bin/bash

AWS_PROFILE=$1
ROUTE53_ZONE_NAME=$2

if [ "X${AWS_PROFILE}" = "X" ] || [ "X${ROUTE53_ZONE_NAME}" = "X" ] ;
then
  echo "Usage: $0 <aws profile> <aws route 53 zone name>"
  exit 1
fi

ROUTE53_ZONE_ID=$(aws --profile ${AWS_PROFILE} route53 list-hosted-zones --output json | jq -r --arg ROUTE53_ZONE_NAME "${ROUTE53_ZONE_NAME}" ".HostedZones[] | select(.Name==\"${ROUTE53_ZONE_NAME}.\") | .Id " | cut -d'/' -f 3)

echo ${ROUTE53_ZONE_ID}
